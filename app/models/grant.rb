class Grant < ActiveRecord::Base

  def self.uniq_rows(column_name)
    Grant.select(column_name).uniq.map(&column_name)
  end

end
