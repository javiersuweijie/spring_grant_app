class GrantsController < ApplicationController
  def index
    grant = Grant.where(grant_params).first
    if grant_params.nil? or grant.nil?
      flash[:error] = 'Error occurred. Please try again.'
      redirect_to search_path
    else
      @grants = grant.results.split('/')
    end
  end

  def grant_params
    params.require(:grant).permit(:reg_sg,:staff,:age,:shareholding,:turnover,:industry,:challenge,:pain_point)
  end
end
