class CreateGrants < ActiveRecord::Migration
  def change
    create_table :grants do |t|
      t.column :reg_sg, :string
      t.column :shareholding, :string
      t.column :staff, :string
      t.column :age, :string
      t.column :turnover, :string
      t.column :industry, :string
      t.column :challenge, :string
      t.column :pain_point, :string
      t.column :results, :string
    end
  end
end
